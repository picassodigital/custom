#!/usr/bin/env bash
:<<\_c
https://storage.googleapis.com/golang/
_c


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_c
mount the 'go' share on host as vagrant/vagrant
_c

function _mount_host_go() {

_debug "_mount_host_go"

[[ -d /go ]] || {

if [[ -n "$1" ]]; then

cat > $HOME/.smbcredentials <<!
username=$1
password=$2
domain=$3
!
sudo chmod 400 $HOME/.smbcredentials
sudo chown $username:$usergroup $HOME/.smbcredentials

else

case "$USER" in

picasso)
cat > $HOME/.smbcredentials <<!
username=$DEFAULT_CIFS_USER
password=$DEFAULT_CIFS_PASS
!
#domain=WORKGROUP
sudo chmod 400 $HOME/.smbcredentials
sudo chown $username:$usergroup $HOME/.smbcredentials
;;

*)
_error "UNKNOWN username: $username"
return 1
;;

esac

fi

sudo mkdir -p /go
sudo chown $USER /go

:<<\_c
noperm - https://superuser.com/questions/744384/allow-chmod-on-cifs-mount
_c
#share=//$HIP/go/$VERSION
share=//$HIP/go
. $REPOV/install/service/samba/install-samba-client.sh $share /go rw,credentials=$HOME/.smbcredentials,uid=1000,nounix,noperm,noserverino,_netdev,iocharset=utf8,file_mode=0777,dir_mode=0777,vers=2.1
:<<\_x
sudo mount //$HIP/go /go -t cifs -o rw,username=kendal,password=dev

mount //192.168.1.8/go /go cifs rw,_netdev,username=kendal,password=dev,iocharset=utf8,nounix,noserverino,file_mode=0777,dir_mode=0777 0 0
sudo cat /etc/fstab
_x
sudo mount /go

#_mount P /go //$HIP/go 

}

}


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
if [[ -n "$*" ]]; then  # do nothing if there are no parameters, that way we can still load the file and call its functions

_debug "type: $1, dest: $2"

case "$1" in

pkg)
. $REPOV/install/sys/lang/go/install.sh
_install_pkg
;;

bin)
. $REPOV/install/sys/lang/go/install.sh
_install_bin
;;

esac

# thus far, Go has only been installed - no mounting of workspace has been done
# therefore, go can't actually be used yet
#go get github.com/rogpeppe/godeps

true  # make sure we return success
fi
